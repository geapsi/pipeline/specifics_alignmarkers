#!/usr/bin/env nextflow

include { extract_snp_context_sequence } from './subworkflows/extract_snp_context_sequence/extract_snp_context_sequence.nf'

include { csvToFasta } from './modules/csvToFasta.nf'
include { getCsvOfMarkers } from './modules/getCsvOfMarkers.nf'
include { fastaPolymorphismPosition } from './modules/fastaPolymorphismPosition.nf'
include { alignmentMinimap } from './modules/alignmentMinimap.nf'
include { filterAlignment } from './modules/filterAlignment.nf'
include { alignmentStatistics } from './modules/alignmentStatistics.nf'
include { alignmentStatistics as filteredAlignmentStatistics } from './modules/alignmentStatistics.nf'

workflow {

	extra_opt = ""

	if (params.filter_evalue){
		extra_opt += " -e "
	}
	if (params.filter_length){
		extra_opt += " -l "
	}
	if (params.filter_indel){
		extra_opt += " -i "
	}
	if (params.filter_polymorphism){
		extra_opt += " -c "
	}

	ch_genome = Channel.fromPath(params.genome)

	if (params.vcf || params.bed || params.markers_csv) {
		if (params.vcf || params.bed) {
			// INPUT IS VCF OR BED
			// If the goal is to extract context sequences from the file and align
			// them on another genome
			if (params.vcf) {
				snp_file = params.vcf
			} else {
				snp_file = params.bed
			}
			Channel
			.fromPath(snp_file, checkIfExists:true)
			.map {
				it ->
				tuple(params.file_prefix, it)
				}
			.set { ch_snp_pos }

			Channel
			.fromPath(params.snp_genome, checkIfExists:true)
			.map {
				it ->
				tuple(params.file_prefix, it)
				}
			.set { ch_snp_genome }
			extract_snp_context_sequence(ch_snp_pos, ch_snp_genome)
			ch_markers_fasta = extract_snp_context_sequence.out.fasta.map { it[1] }
		}
		if (params.vcf) {
			getCsvOfMarkers(extract_snp_context_sequence.out.fasta)
			// gets the polymorphism inside brackets in the seq
			ch_markers_csv = getCsvOfMarkers.out.csv.map { it[1] }
		} else if (params.markers_csv) {
			// INPUT IS CSV
			ch_markers_csv = Channel.fromPath(params.markers_csv)
		}
		if (params.vcf || params.markers_csv){
			csvToFasta(ch_markers_csv)
			ch_markers_fasta = csvToFasta.out.fasta
		}
	} else if (params.markers_fasta) {
		// INPUT IS FASTA
		ch_markers_fasta = Channel.fromPath(params.markers_fasta)
	} else {
		println("Input markers either in CSV, FASTA or VCF/BED format") // TODO nice error message
	}

	if (params.polymorphism && (params.vcf || params.markers_csv || params.markers_fasta)) {
	fastaPolymorphismPosition(ch_markers_fasta)

	alignmentMinimap(ch_genome, fastaPolymorphismPosition.out.fasta_markers_N)
	alignmentStatistics(alignmentMinimap.out.paf, 'False')
	filterAlignment(fastaPolymorphismPosition.out.fasta_markers_N, alignmentMinimap.out.paf, fastaPolymorphismPosition.out.tsv_markers, extra_opt)
	filteredAlignmentStatistics(filterAlignment.out.filtered_paf, 'True')
	} else if (!params.polymorphism) {
		// we dont have alleles for the BED and sometimes for the CSV and VCF
		alignmentMinimap(ch_genome, ch_markers_fasta)
		alignmentStatistics(alignmentMinimap.out.paf, 'False')
		ch_no_poly_file = Channel.fromPath('conf/None')
		filterAlignment(ch_markers_fasta, alignmentMinimap.out.paf, ch_no_poly_file, extra_opt)
		filteredAlignmentStatistics(filterAlignment.out.filtered_paf, 'True')
	}
}

