// Global default params, used in configs
params {

	// Mandatory parameters
		// Reference genome on which to perform the alignment
			genome = 'example_dataset/Lcu.2RBY_chr1.fasta.gz' // genome on which to transfer sequences
		// For the markers, either:
			// if the polymorphism information is included (inside brackets in the CSV/FASTA or in the VCF)
				params.polymorphism = false
			// CSV of markers without a header (id, seq)
				markers_csv = ''
			// OR a fasta of the markers to skip the csvToFasta process
				markers_fasta = ''
			// OR a VCF/BED along with the genome on which positions were originally identified
				vcf = ''
				bed = 'example_dataset/neupane2022.bed.gz'
				snp_genome = 'example_dataset/Lcu.2RBY_chr1.fasta.gz'

	// Non-mandatory parameters
		// Prefix to assign to files (to avoid name collision)
                file_prefix = 'example_bed'
		// How much to extend the context sequence from the SNP marker in the extracted sequence
			slop_extend_size = 100
		// Option to filter by evalue
			filter_evalue = false
		// Option to filter by length
			filter_length = false
		// Option to filter alignments with indel
			filter_indel = true
		// Option to filter by polymorphism correct
			filter_polymorphism = false
		// Option to show stats (histrogram, boxplot)
			stats = false
	// Output options
		project_dir = projectDir
		publish_dir_mode = 'copy'
		// Name of the output directory
			outdir = 'results_example_bed'


}

// get detailed timeline of the pipeline execution
timeline {
	enabled = true
	file = "$params.outdir/timeline.html"
}

// get detailed execution report
report {
		enabled = true
		file = "$params.outdir/report.html"
}

conda {
enabled = true
useMamba = false
}
