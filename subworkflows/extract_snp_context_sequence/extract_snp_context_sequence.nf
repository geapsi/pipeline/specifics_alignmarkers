#!/usr/bin/env nextflow

include { gunzip } from '../../modules/gunzip.nf'
include { bcftoolsAnnotate as annotatevcf } from '../../modules/bcftoolsAnnotate.nf'
include { bedopsVcf2bed as vcf2bed } from '../../modules/bedopsVcf2bed.nf'
include { samtoolsFaidx as faidx } from '../../modules/samtoolsFaidx.nf'
include { bedtoolsSlop as slop } from '../../modules/bedtoolsSlop.nf'
include { bedtoolsGetfasta as getfasta } from '../../modules/bedtoolsGetfasta.nf'
// include { getCsvOfMarkers as csvmarkers } from '../../modules/getCsvOfMarkers.nf'


    workflow extract_snp_context_sequence {

        take:
            snp_pos // tuple(id, file), file: path/to/file.vcf or .bed
            genome // tuple (id, file), file: path/to/file.fasta

        main:

            if (params.vcf) {
                if (params.add_ids_to_vcf) {
                    annotatevcf(snp_pos)
                    ch_vcf = annotatevcf.out.vcf
                } else {
                    ch_vcf = snp_pos
                }
                // necessary to have start/end fields for bedtools slop
                vcf2bed(ch_vcf)
                ch_bed = vcf2bed.out.bed
            } else if (params.bed) {
                ch_bed = snp_pos
            } else {
                println "Please provide SNPs either in VCF or BED format"
            }
            ch_genome = genome
            // faidx cannot deal with gz files
            if (params.snp_genome.endsWith('.gz')) {
                gunzip(genome)
                ch_genome = gunzip.out.gunzip
            }
            faidx(ch_genome)
            slop(ch_bed.join(faidx.out.fai))
            getfasta(ch_genome.join(slop.out.bed))
            // csvmarkers(getfasta.out.fasta)

        emit:
            fasta = getfasta.out.fasta
        }