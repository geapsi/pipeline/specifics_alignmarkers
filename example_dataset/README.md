# Example dataset 

Nomenclature for the test sequences: 
- **test**(name)
- **_(+ or -)**(strand alignmenent)
- **_X**(Beginning of the alignment)**_X**(End of the alignment)
- **_P1S**(P = polymorphism, Number = Position of the polymorphism, S = Value of the polymorphism (IUPAC Code))
- **_D10-14**(D = Deletion (gap insertion on the markers), 10 = first position of the deletion, 14 = last position of the deletion, Between this interval there is a deletion)
- **_I15-18**(I = Insertion (gap insertion on the reference), 15 = first position of the insertion,18 = last position of the insertion, between this interval there is an insertion)
- **_M25TA**(M = mismatch, 25 = Position of the mismatch, T = Base on the reference, A = base on the query)

The _neg means the alignment should not work 

- test_+_0_180_P1S : Test to see the position of the polymorphism strand +

- test_-_0_180_P1S : Test to see the position of the polymorphism strand -

- test_-_120_240_P86W : Test for polymorphism detection on strand -

- test_-_0_120_P54S : Test for polymorphism detection on strand -

- test_neg : It shouldn't match any polymorphism and not in the genome

- test_neg_+_1080_1200_P53M : It shouldn't match, not the right polymorphism

- test_neg_-_1380_1500_P50R : It shouldn't match, not the right polymorphism

- test_+_520_660 : Test the detection of extreme position polymorphism on the strand (not all of the marker is aligned and the polymorphism is on the part which is not aligned)

- test_+_420_519 : Test the detection of extreme position polymorphism on the strand (Not all of the marker is aligned and the polymorphism is on the part which is not aligned)

- test_+_0_120_CI18-20_P40M : Test IUPAC code recognition and processing ability (CI = code IUPAC a case to see how it's treated)

- test_+_480_600_P39M : Test for polymorphism detection on strand +

- test_+_1680_180_I60-65_P96M : Test bases insertion on the detection of polymorphism

- test_+_300_480_P47_D19-21 : Test bases deletion on the detection of polymorphism

- test_+_600_720_M57TA_M58TA_M59TA_P112Y : Test bases mismatch on the detection of polymorphism

- test_+_1020_1200_P106S_D9-10_D17-20_I52-53 : Test bases deletion and insertion on the detection of polymorphism

- test_-_1020_1140_PY0 : Test for polymorphism in the first position

- test_-_1020_1140_P118Y : Test for polymorphism in the second position

- test_-_1020_1140_P1K : Test for polymorphism detection on complementary strand

- test_-_1320_1440_D92-94_P4Y : Test the detection of polymorphism on complementary strands when there is a deletion

- test_-_1920_2100_D34-36_D156-158_P95Y : Test the detection of polymorphism on complementary strands when there are multiple deletions

- test_-_3120_3300_P88B_D125-127_I162-164 : Test the detection of polymorphism on complementary strands when there are multiple deletions and insertions

- test_-_480_660_P172S_D36-39_I80-83 : Test strand - where there is case of multiple deletion before the polymorphism

- test_-_480_660_P124S_D152-156_I168-170 : Test strand - where there is case of multiple deletion after the polymorphism

- test_-_3120_3300_P88V_D31-34_D116-127_D148-150_I162-164 : Test strand - where there is case of multiple deletions after the polymorphism

