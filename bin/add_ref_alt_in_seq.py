#!/usr/bin/env python3

import argparse
import re

from Bio import SeqIO

parser = argparse.ArgumentParser(description = "Adds the polymorphism inside the sequence \
    using information from the header provided by bedtools getFasta")
parser.add_argument('-fasta', help = 'markers file format fasta', required=True)
parser.add_argument('-slop_size', help = 'markers file format fasta', required=True,
    type=int)
parser.add_argument('-output', help = 'csv file with marker in first column and \
    sequence with variants in the second', default='markers.csv', required=False)
args = parser.parse_args()

def extract_snp_info_from_header(header, slop_size):
    """ Extract SNP information from the fasta header
    @param header    a record.id from Bio SeqIO with the format CHR_POS_REF_ALT

    @return  a dictionnary with SNP ID, ref, alt and position
        relative to the sequence start in the fasta
    """
    d = {}
    d['id'] = str(header)
    d['ref'] = d['id'].split('_')[-2]
    d['alt'] = d['id'].split('_')[-1]
    d['relative_pos'] = slop_size
    return d

def insert_ref_alt_in_seq(seq, d):
    """ Replace nucleotide(s) of ref by [ref/alt] in the sequence
    @param seq  a record.seq from Bio.SeqIO
    @param d    a dic with info on the polymorphism (id, ref, alt, relative_pos)

    @return  the sequence having the polymorphism included between squared brackets
    """
    seq_after_poly = len(seq) - d['relative_pos']
    if (len(d['ref']) >= seq_after_poly) or (len(d['alt']) >= seq_after_poly):
        raise ValueError(f"""The ref or alt version of the SNP is longer than
        the sequence after the position of the variant. Check SNP {d['id']} \
        and probably extend the slop parameter.""")
    # in case it is not a single nucleotide polymorphism
    replace_until = d['relative_pos'] + len(d['ref'])
    poly_ref_alt = '[' + d['ref'] + '/' + d['alt'] + ']'
    seq_with_poly = str(seq[:d['relative_pos']] + poly_ref_alt + seq[replace_until:])
    return seq_with_poly

if __name__ == "__main__":

    with open(args.output, 'w', encoding='utf-8') as ofl:
        for record in SeqIO.parse(args.fasta, 'fasta'):
            d_snp = extract_snp_info_from_header(record.id, args.slop_size)
            ofl.write(d_snp['id'] + ',' + insert_ref_alt_in_seq(record.seq, d_snp) + '\n')