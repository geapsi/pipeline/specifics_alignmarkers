#!/usr/bin/env python3
"""
Created on 20/04/21

Script to filter alignment file file obtained from QTLs markers alignment to vfab genome and add some statistics like which markers wasn't aligned on the genome

conda activate python3_basics
Usage: python3 filterBlastn.py -markersf -alignmentf -outfile -eval_filter -qlen_filter -check_markeur -log_file -statistics
"""

import argparse
import logging
import time

import pandas as pd

from Bio import SeqIO
from Bio.Seq import Seq

import code_iupac as ci
from paf_parsing import parse_paf_into_df
from paf_parsing import add_alignment_statistic_columns

pd.set_option('display.max_columns', None)

parser = argparse.ArgumentParser(description ='Filter a paf alignment file\
    depending on various statistics')
parser.add_argument('-m', '--markers',
    help='markers in fasta format with a N for the polymorphism',
    required=True)
parser.add_argument('-a', '--alignment',
    help='paf alignement file',
    required=True)
parser.add_argument('-p', '--polymorphism_markers',
    help='3-cols tsv file with the id of the polymorphism,\
        its position and its value (IUPAC) in the query',
    required=True)
parser.add_argument('-e', '--eval_filter',
    help='if duplicated rows, keep the one with the lowest evalue (step 1)',
    required=False, action= 'store_true')
parser.add_argument('-l', '--qlen_filter',
    help='if duplicated rows, keep where query length is the highest (step 2)',
    required=False, action= 'store_true')
parser.add_argument('-i','--indel_filter',
    help='removes the alignment if there are insertions or deletions',
    required=False, action='store_true')
parser.add_argument('-c','--correct_polymorphism_filter',
    help='removes alignments where the polymorphism detected is not as expected',
    required=False, action='store_true')
parser.add_argument('-f', '--output_filtered_paf',
    help='filtered paf in csv format',
    required=False, default='filtered_paf.csv')
parser.add_argument('-n', '--output_polymorphism_nature',
    help='output information on the polymorphism position and value on query and subject',
    required=False, default='position_marker.tsv')
parser.add_argument('-s', '--output_alignment_sequences',
    help='output aligned sequences of query and subject with the polymorphism values',
    required=False, default='alignment_query_subject_sequences.txt')
parser.add_argument('--logs',
    help='output log files regarding unaligned markers',
    required=False, default='unaligned_markers.log')
args = parser.parse_args()

def filter_by_score(df_alignment):
    """ Filter a PAF alignment based on the Smith Waterman alignment score to keep
    the best alignment for each marker (no loss)
    @param df_alignment a pd df of the PAF alignment file
    @return df_filtered  a df filtered on the score
    """
    row_ids = df_alignment.groupby('qseqid')['score'].transform(max) == df_alignment['score']
    df_filtered = df_alignment[row_ids]
    return(df_filtered)

def filter_by_qlength(df_alignment):
    """ Filter a PAF alignment based on the alignment length to keep the best
    alignment for each marker (no loss)
    @param df_alignment a pd df of the PAF alignment file
    @return df_filtered  a df filtered on the alignment length
    """
    row_ids = df_alignment.groupby('qseqid')['length'].transform(max) == df_alignment['length']
    df_filtered = df_alignment[row_ids]
    return(df_filtered)

def log_markers_not_aligned_log_file(set_element_remaining):
    """
    For a given set marker display it
    """
    print(set_element_remaining)
    logging.info("There are %s markers that weren't aligned", len(set_element_remaining))
    for i in set_element_remaining:
        logging.info('A marker was not aligned on the genome : %s', i)
    return None

def get_df_marker_id_seq(fasta_file):
    """ Create a pd df from a fasta file
    @param fasta_file an opened fasta file of marker sequences with a N in place
        of the polymorphism
    @return df_seq  a df with the marker id in first col and seq in second col
    """
    d_id_seq = {'qseqid' : [], 'qseq' : []}
    for record in SeqIO.parse(fasta_file, 'fasta'):
        d_id_seq['qseqid'].append(str(record.id).strip())
        d_id_seq['qseq'].append(str(record.seq).strip())
    df_seq = pd.DataFrame.from_dict(d_id_seq)
    return df_seq

def get_df_marker_position_value(file_marker_polymorphism):
    """ Create a pd df from a marker file describing the polymorphism
    @param file_marker_polymorphism an opened marker txt file space separated,
        with the id, the position of the polymorphism and its IUPAC value
    @return df_seq  a df with the marker id in first col and seq in second col
    """
    d_pos_value = {'qseqid' : [], 'pos_poly' : [], 'value_poly' : []}
    for row in file_pos_value:
        l_row = row.split('\t')
        d_pos_value['qseqid'].append(l_row[0].strip())
        d_pos_value['pos_poly'].append(l_row[1].strip())
        d_pos_value['value_poly'].append(l_row[2].strip())
    df_pos_value = pd.DataFrame.from_dict(d_pos_value)
    return df_pos_value

def get_polymorphism_position(pos_poly, qstart, qend):
    """ Computes the position of the polymorphism in the alignment
    @param pos_poly position of the polymorphism in the marker sequence
    @param qstart query start
    @param qend query end
    @param pos_poly_on_alignment position of the polymorphism in the alignement
    @return pos_poly_on_alignment either -1 if the polymorphism is out of the alignement
        or the position of the polymorphism is the alignement
    """
    if int(pos_poly) <= int(qstart) or int(pos_poly) >= int(qend):
        pos_poly_on_alignment = -1
    else:
        pos_poly_on_alignment = int(pos_poly) - int(qstart)
    return pos_poly_on_alignment

def handle_mismatch(cs_row, seq_ref, i, processed_seq_last_index):
    """ Process mismatch in a PAF cigar. A mismtach can be "*gn" where a G in the ref
    was replaced by a N in the query
    @param cs_row the full cigar
    @param seq_ref the already processed part of the cigar
    @param i the character being read in the cigar
    @param processed_seq_last_index index of the last nucleotide in seq_ref
    @return updated seq_ref, i, seq_len
    """
    seq_ref += cs_row[i+1].upper()
    i += 3
    processed_seq_last_index += 1
    return seq_ref, i, processed_seq_last_index

def handle_equal(eq_bool, min_bool, plus_bool, i):
    """ Register equality in a PAF cigar.
    @param eq_bool is equal boolean
    @param min_bool is deletion boolean
    @param plus_bool is insertion boolean
    @param i the character being read in the cigar
    @return updated eq_bool, min_bool, plus_bool, i
    """
    eq_bool = True
    min_bool = False
    plus_bool = False
    i += 1
    return eq_bool, min_bool, plus_bool, i

def handle_deletion(eq_bool, min_bool, plus_bool, i):
    """ Register deletion in a PAF cigar.
    @param eq_bool is equal boolean
    @param min_bool is deletion boolean
    @param plus_bool is insertion boolean
    @param i the character being read in the cigar
    @return updated eq_bool, min_bool, plus_bool, i
    """
    min_bool = True
    eq_bool = False
    plus_bool = False
    i += 1
    return eq_bool, min_bool, plus_bool, i

def handle_insertion(eq_bool, min_bool, plus_bool, i):
    """ Register insertion in a PAF cigar.
    @param eq_bool is equal boolean
    @param min_bool is deletion boolean
    @param plus_bool is insertion boolean
    @param i the character being read in the cigar
    @return updated eq_bool, min_bool, plus_bool, i
    """
    plus_bool = True
    eq_bool = False
    min_bool = False
    i += 1
    return eq_bool, min_bool, plus_bool, i

def treat_identity(cs_row, seq_ref, i, processed_seq_last_index):
    """ Process identity in a PAF cigar.
    @param cs_row the full cigar
    @param seq_ref the already processed part of the cigar
    @param i the character being read in the cigar
    @param processed_seq_last_index index of the last nucleotide in seq_ref
    @return updated seq_ref, i, processed_seq_last_index
    """
    seq_ref += cs_row[i]
    i += 1
    processed_seq_last_index += 1
    return seq_ref, i, processed_seq_last_index

def treat_deletion(cs_row, seq_ref, i, processed_seq_last_index, qseq_row,
    pos_poly_on_alignment, strand, filter_indel):
    """ Process deletion in a PAF cigar.
    @param cs_row the full cigar
    @param seq_ref the already processed part of the cigar
    @param i the character being read in the cigar
    @param processed_seq_last_index index of the last nucleotide in seq_ref
    @qseq_row the query sequence
    @param pos_poly_on_alignment position of the polymorphism in the alignment
    @param strand strand of the alignment
    @param filter_indel to mark sequences with indel
    @return updated seq_ref, i, processed_seq_last_index, pos_poly_on_alignment, qseq_roweq_bool
    """
    if filter_indel:
        pos_poly_on_alignment = -1
    if pos_poly_on_alignment != -1:
        if strand == '-' and i > ((len(qseq_row)-1) - pos_poly_on_alignment):
            pos_poly_on_alignment += 1
        if strand == '+' and pos_poly_on_alignment > i:
            pos_poly_on_alignment += 1
    qseq_row = qseq_row[:processed_seq_last_index+1] + '-' + qseq_row[processed_seq_last_index+1:]
    seq_ref += cs_row[i].upper()
    i += 1
    processed_seq_last_index += 1
    return seq_ref, i, processed_seq_last_index, pos_poly_on_alignment, qseq_row

def treat_insertion(seq_ref, i, processed_seq_last_index, pos_poly_on_alignment, strand, filter_indel):
    """ Process insertion in a PAF cigar.
    @param seq_ref the already processed part of the cigar
    @param i the character being read in the cigar
    @param processed_seq_last_index index of the last nucleotide in seq_ref
    @param pos_poly_on_alignment position of the polymorphism in the alignment
    @param strand strand of the alignment
    @param filter_indel to mark sequences with indel
    @return updated seq_ref, i, processed_seq_last_index, pos_poly_on_alignment
    """
    if filter_indel:
        pos_poly_on_alignment = -1
    if pos_poly_on_alignment != -1:
        if strand == '-':
            pos_poly_on_alignment -= 1
    seq_ref += '-'
    i += 1
    processed_seq_last_index += 1
    return seq_ref, i, processed_seq_last_index, pos_poly_on_alignment

def treat_cigar(cs_row, strand, pos_poly_on_alignment, qseq_row, filter_indel):
    """ Parse the PAF cigar
    @param cs_row the full cigar
    @param strand strand of the alignment
    @param pos_poly_on_alignment position of the polymorphism in the alignment
    @param qseq_row query sequence
    @param filter_indel to mark sequences with indel
    @return qseq_row, seq_ref (with N replaced), pos_poly_on_alignment
    """
    # the sequence of the ref to construct using the CIGAR
    seq_ref = ''
    # minus 1 because the cigar starts with an '=' sign
    actual_seq_length = -1
    eq_bool = False
    min_bool = False
    plus_bool = False

    # examining the cigar character by character
    i = 0
    while i < len(cs_row):
        if cs_row[i] == '*':
            seq_ref, i, actual_seq_length = handle_mismatch(cs_row, seq_ref, i, actual_seq_length)
        elif cs_row[i] == '=':
            eq_bool, min_bool, plus_bool, i = handle_equal(eq_bool, min_bool, plus_bool, i)
        elif cs_row[i] == '-':
            eq_bool, min_bool, plus_bool, i = handle_deletion(eq_bool, min_bool, plus_bool, i)
        elif cs_row[i] == '+':
            eq_bool, min_bool, plus_bool, i = handle_insertion(eq_bool, min_bool, plus_bool, i)
        elif eq_bool:
            seq_ref, i, actual_seq_length = treat_identity(cs_row, seq_ref, i, actual_seq_length)
        elif min_bool:
            seq_ref, i, actual_seq_length, pos_poly_on_alignment , qseq_row = treat_deletion(
                cs_row, seq_ref, i, actual_seq_length, qseq_row,
                pos_poly_on_alignment, strand, filter_indel)
        elif plus_bool:
            seq_ref, i, actual_seq_length, pos_poly_on_alignment = treat_insertion(seq_ref,
                i, actual_seq_length, pos_poly_on_alignment, strand, filter_indel)
    return qseq_row, seq_ref, pos_poly_on_alignment

def check_polymorphism(df, filter_indel):
    """ Analyzing the polymorphism position and value taking into account indels
    @param df a pd dataframe of the markers
    @param filter_indel to mark sequences with indel
    @return df  a modified version of the original df with new info on the polymorphism
    """
    df['pos_poly_on_alignment'] = 0
    df['poly_value_is_correct'] = 0
    df['poly_value_subject'] = ''
    df['pos_poly_on_subject'] = 0
    for index, row in df.iterrows():
        pos_poly_on_alignment = get_polymorphism_position(row['pos_poly'], row['qstart'], row['qend'])
        if row['strand'] == '-':
            qseq_row = str(Seq(str(row['qseq'])).reverse_complement())
            value_poly = str(Seq(str(row['value_poly'])).complement())
            df.at[index, 'value_poly'] = value_poly
        else:
            qseq_row = row['qseq']
        # seq_ref is the aligned region of the ref on the query (can contain some '-')
        qseq_row, seq_ref, pos_poly_on_alignment = treat_cigar(row['cs'], row['strand'],
            pos_poly_on_alignment, qseq_row, filter_indel)

        df.at[index, 'qseq_ali'] = qseq_row
        df.at[index, 'rseq_ali'] = seq_ref
        # check if the expected polymorphism is found in the reference
        if row['strand'] == '-':
            len_right_side_of_poly = int(row['send']) - int(row['sstart']) - pos_poly_on_alignment - 1
            if not ((pos_poly_on_alignment + 1) >= (len(seq_ref))):
                # if -1 was removed because of filter_indel
                # the expected value of the polymorphism is not found on the ref
                subject_seq_length = int(row['send']) - int(row['sstart'])
                if pos_poly_on_alignment != -1 and ci.verif_iupac(value_poly, seq_ref[len_right_side_of_poly]) is True:
                    df.at[index, 'poly_value_is_correct'] = int(-2)
                    df.at[index, 'pos_poly_on_alignment'] = subject_seq_length - pos_poly_on_alignment - 1
                    df.at[index, 'pos_poly_on_subject'] = int(row['sstart']) + len_right_side_of_poly
                    df.at[index, 'poly_value_subject'] = seq_ref[len_right_side_of_poly]
                # the expected value of the polymorphism is not found on the ref
                elif pos_poly_on_alignment != -1 and ci.verif_iupac(value_poly, seq_ref[len_right_side_of_poly]) is False:
                    df.at[index, 'poly_value_is_correct'] = int(-3)
                    df.at[index, 'pos_poly_on_alignment'] = subject_seq_length - pos_poly_on_alignment - 1
                else:
                    df.at[index, 'poly_value_is_correct'] = int(-3)
                    df.at[index, 'pos_poly_on_alignment'] = pos_poly_on_alignment
        else:
            if pos_poly_on_alignment != -1 and ci.verif_iupac(row['value_poly'], seq_ref[pos_poly_on_alignment]):
                df.at[index, 'poly_value_is_correct'] = int(-2)
                df.at[index, 'pos_poly_on_alignment'] = pos_poly_on_alignment
                df.at[index, 'poly_value_subject'] = seq_ref[pos_poly_on_alignment]
                df.at[index, 'pos_poly_on_subject'] = int(row['sstart']) + pos_poly_on_alignment
            else:
                df.at[index, 'poly_value_is_correct'] = int(-3)
                df.at[index, 'pos_poly_on_alignment'] = pos_poly_on_alignment
    return df

def remove_markers_with_incorrect_polymorphism(df, filter_incorrect_poly):
    """ Some alignments show indel which are difficult to work with, remove them
        if filter_incorrect_poly.
    Removing them from the dataframe.
    @param df df of the markers
    @param filter_incorrect_poly to ignore markers with incorrect poly
    @return df_poly  same as df but with marker with indel removed and the
    poly_value_subject column removed
    """
    df_poly = df.copy()
    if filter_incorrect_poly:
        df_poly = df_poly.loc[(df_poly['poly_value_is_correct'] == -2),]
    df_poly.drop('poly_value_is_correct', axis=1, inplace=True)
    return df_poly

def output_alignment_sequences(df, outfile):
    """ Write sequences of the query, the subject, the alignment, and the polymorphism
    @param df df of the markers
    @return outfile  an opened file to write to
    """
    for _, row in df.iterrows():
        pos_poly = row['pos_poly_on_alignment']
        outfile.write(f"{row['qseqid']} | poly at {pos_poly} with value {row['value_poly']} ; "
            f"{row['strand']} strand ; expected poly found ({row['poly_value_is_correct']})\n")
        outfile.write(f"cigar :\n{row['cs']}\n")
        outfile.write(f"qseq:\n{row['qseq']}\n")
        outfile.write(f"qseq_ali:\n{row['qseq_ali']}\n")
        outfile.write(f"rseq_ali:\n{row['rseq_ali']}\n")
        outfile.write(f"seq match:\n{row['rseq_ali'][pos_poly - 2 : pos_poly + 3]}\n")

if __name__ == "__main__":
    start_time = time.time()

    with open(args.alignment, 'r', encoding='utf-8') as paf:
        df_original = parse_paf_into_df(paf)

    # create basic alignment statistic columns
    df_original = add_alignment_statistic_columns(df_original)
    # FILTERING BASED ON ALIGNMENT LENGHT AND EVALUE
    if args.qlen_filter and args.eval_filter:
        print('Filtered by length and evalue')
        df_filtered = filter_by_qlength(df_original)
        df_filtered = filter_by_score(df_filtered)
    elif args.qlen_filter:
        print('Filtered by length')
        df_filtered = filter_by_qlength(df_original)
    elif args.eval_filter:
        print('Filtered by evalue')
        df_filtered = filter_by_score(df_original)
    else:
        df_filtered = df_original
    # EXTRACT POLYMORPHISM INFO AND EVENTUALLY FILTER BASE ON EXPECTATIONS
    with open(args.markers, 'r', encoding='utf-8') as fasta_markers:
        df_seq = get_df_marker_id_seq(fasta_markers)
    if args.polymorphism_markers and args.polymorphism_markers != 'None': # nextflow optional input
        with open(args.polymorphism_markers, 'r', encoding='utf-8') as file_pos_value:
            df_pos_value_poly = get_df_marker_position_value(file_pos_value)

        df_filtered = pd.merge(df_filtered, df_seq, on='qseqid')
        df_filtered = pd.merge(df_filtered, df_pos_value_poly, on='qseqid')
        # removing the cs:Z prefix
        df_filtered['cs'] = df_filtered['cs'].apply(lambda x: x[5:])
        # extracting the subsequence of qseq TODO -> can do it better
        df_filtered['qseq'] = df_filtered.apply(
            lambda x: x['qseq'][int(x['qstart']):(int(x['qend']))], axis=1)
        # treat the cigar to have the reference sequence and the markers sequence
        df_filtered = check_polymorphism(df_filtered, args.indel_filter)
        with open(args.output_alignment_sequences,
            'w', encoding='utf-8') as outfile_alignment_sequences:
            output_alignment_sequences(df_filtered, outfile_alignment_sequences)
        df_filtered = remove_markers_with_incorrect_polymorphism(df_filtered,
            args.correct_polymorphism_filter)

        # WRITE POLYMORPHISM
        polymorphism_cols = ['sseqid', 'sstart', 'send', 'qseqid', 'strand',
            'pos_poly_on_alignment', 'pos_poly_on_subject', 'poly_value_subject', 'value_poly',
            'pident','plength']
        df_filtered[polymorphism_cols].to_csv(args.output_polymorphism_nature,
            index=False, header=True, sep='\t')

    # WRITE FILTERED PAF
    paf_columns = ['qseqid', 'qlength', 'qstart', 'qend', 'strand',
    'sseqid', 'sstart', 'send', 'match_bases', 'total_bases',
    'score', 'cs']
    df_filtered[paf_columns].to_csv(args.output_filtered_paf, index=False)

    # Missing markers (not aligned, or else TODO)
    logging.basicConfig(level=logging.INFO, filename=args.logs, filemode='w')
    df_original_markers = df_filtered.loc[:,'qseqid']
    list_original_markers = df_original_markers.tolist()
    # we make a set for each list to remove every markers who are multiples times in the file
    s_files_markers = set(df_seq['qseqid'])
    s_original_markers = set(list_original_markers)
    s_element_remaining = sorted(s_files_markers.difference(s_original_markers))
    # for each element of the set, we put them on the log file
    log_markers_not_aligned_log_file(s_element_remaining)