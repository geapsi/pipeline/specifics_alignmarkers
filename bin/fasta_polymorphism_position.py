#!/usr/bin/env python3

import argparse
import re

from Bio import SeqIO
from Bio.Seq import Seq

import code_iupac as ci

parser = argparse.ArgumentParser(description = 'Gets the position of the polymorphism \
    in the sequence and replaces it in the sequence by a N')
parser.add_argument('-m', '--markers', help = 'markers file format fasta', required=True)
parser.add_argument('--output_fasta_n', help = 'output file name for the fasta\
    file with the polymorphism ("[X/X]") replaced by N', required=False, default='markers.fa')
parser.add_argument('--output_marker_position', help = 'output file name for the tsv\
    with polymorphism value and position', required=False, default='marker_position.tsv')
args = parser.parse_args()


def extract_poly_info(marker_id, marker_seq):
    """Takes a csv file and return a fasta
    @param marker_id the identifier of the marker
    @param marker_seq the sequence of the marker with the poly as [X/Y]
    @return d a dic with the position and value of the polymorphism
    """
    d = {}
    d['id'] = marker_id
    print(marker_id)
    poly_format = re.search(r'\[(.*?)\]', str(marker_seq))
    if not poly_format:
        return None
    poly_iupac = ci.to_iupac(poly_format.group(1))
    d['value_poly'] = poly_iupac
    d['pos_poly'] = record.seq.find('[')
    return d

def replace_poly_by_n(marker_seq):
    """Takes a csv file and return a fasta
    @param marker_seq the identifier of the marker
    @return seq_no_poly the same as marker_seq but with the poly replaced by 'N'
    """
    poly_format = re.compile(r'\[.*?\]')
    seq_no_poly = poly_format.sub('N', str(marker_seq))
    return Seq(seq_no_poly)

if __name__ == "__main__":

    with open(args.output_fasta_n, 'w', encoding='utf-8') as out_fasta:
        with open(args.output_marker_position, 'w', encoding='utf-8') as out_poly:
            with open(args.markers, 'r', encoding='utf-8-sig') as markers_fasta:
                for record in SeqIO.parse(markers_fasta, 'fasta'):
                    # write the fasta with N in place of the polymorphism
                    sequence_n = SeqIO.SeqRecord(replace_poly_by_n(record.seq),
                        id=record.id, description='')
                    SeqIO.write(sequence_n, out_fasta, 'fasta')
                    d_poly_value_pos = extract_poly_info(record.id, record.seq)
                    if d_poly_value_pos is not None:
                        out_poly.write(str(d_poly_value_pos['id']) + '\t' +
                            str(d_poly_value_pos['pos_poly']) + '\t' +
                            str(d_poly_value_pos['value_poly']) + '\n')