#!/usr/bin/env python3

import pandas as pd

def parse_paf_into_df(paf_file):
    """ Parse a PAF file into a pd df
    @param df_alignment an opened PAF file
    @return df_paf  a pd df of the paf_file
    """
    d_paf = { 'qseqid' : [], 'qlength' : [], 'qstart' : [], 'qend' : [],
        'strand' : [], 'sseqid' : [], 'sstart' : [], 'send' : [],
        'match_bases' : [], 'total_bases' : [], 'score' : [], 'cs' : []}
    for row in paf_file:
        l_row = row.split('\t')
        d_paf['qseqid'].append(l_row[0].strip())
        d_paf['qlength'].append(int(l_row[1].strip()))
        d_paf['qstart'].append(int(l_row[2].strip()))
        d_paf['qend'].append(int(l_row[3].strip()))
        d_paf['strand'].append(l_row[4].strip())
        d_paf['sseqid'].append(l_row[5].strip())
        d_paf['sstart'].append(int(l_row[7].strip()))
        d_paf['send'].append(int(l_row[8].strip()))
        d_paf['match_bases'].append(int(l_row[9].strip()))
        d_paf['total_bases'].append(int(l_row[10].strip()))
        d_paf['score'].append(l_row[14].strip())
        d_paf['cs'].append(l_row[-1].strip())
    df_paf = pd.DataFrame.from_dict(d_paf)
    return df_paf

def add_alignment_statistic_columns(df_paf):
    """ Create alignment stats expected in most format, including alignment length
    percent of identity and percentage of the length of query found
    @param df_paf a pd df of the PAF alignment file
    @return df_paf_stats  a df filtered on the score
    """
    df_paf_stats = df_paf.copy()
    df_paf_stats['length'] = df_paf_stats['qend'] - df_paf_stats['qstart']
    df_paf_stats['pident'] = (df_paf_stats['match_bases'] / df_paf_stats['total_bases']) * 100
    df_paf_stats['plength'] = (df_paf_stats['length'] / df_paf_stats['qlength']) * 100
    # removing the AS:i: prefix
    df_paf_stats['score'] = df_paf_stats['score'].astype(str).apply(lambda x: x[5:])
    return df_paf_stats