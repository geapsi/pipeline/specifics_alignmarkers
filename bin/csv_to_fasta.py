#!/usr/bin/env python3
import argparse


parser = argparse.ArgumentParser(description = 'Convert CSV file to fasta')

parser.add_argument('-markers_csv', help='input of markers in csv format. 2 cols, with\
    markers first and then their sequence with the polymorphism formated with [A/G]',
    required=True)
parser.add_argument('-sep', help='separator of the marker file if not csv',
    required=False, default=',')
parser.add_argument('-output_fasta', help='output of markers in fasta format',
    required=False, default='conv_csv_fasta.fasta')
args=parser.parse_args()



def convert_csv_to_fasta(csv, fasta, sep):
    """Takes a csv file and return a fasta
    @param csv an opened csv file
    @param fasta an opened output to write the fasta file
    @param sep which separator in your csv files you use between row
    """
    # number of sequence characters before folding
    seq_fold = 60

    for line in csv:
        row = line.replace('"', '').split(sep, 1)
        marker_id = row[0].strip()
        seq = row[1].strip()
        fasta.write('>' + marker_id + '\n')
        for subseq in range(0, len(seq), seq_fold):
            fasta.write(seq[subseq : subseq + seq_fold].strip() + '\n')

if __name__ == "__main__":

    with open(args.output_fasta, 'w', encoding='utf-8') as out_markers_fa:
        with open(args.markers_csv, 'r', encoding='utf-8-sig') as markers_csv:
            convert_csv_to_fasta(markers_csv, out_markers_fa, args.sep)
