#!/usr/bin/env python3

from Bio.Data import IUPACData

def verif_iupac(base_iupac,base):
    iupac_base = {
        "A" : ["A"],
        "T" : ["T"],
        "G" : ["G"],
        "C" : ["C"],
        "R" : ["A","G"],
        "Y" : ["C","T"],
        "S" : ["G","C"],
        "W" : ["A","T"],
        "K" : ["G","T"],
        "M" : ["A","C"],
        "B" : ["C","G","T"],
        "D" : ["A","G","T"],
        "H" : ["A","C","T"],
        "V" : ["A","C","G"],
        "N" : ["A","T","C","G"]
    }
    return bool(base in iupac_base[base_iupac])

def to_iupac(poly):
    d_iupac_code = {
        "A/G" : "R",
        "G/A" : "R",
        "C/T" : "Y",
        "T/C" : "Y",
        "G/C" : "S",
        "C/G" : "S",
        "A/T" : "W",
        "T/A" : "W",
        "T/G" : "K",
        "G/T" : "K",
        "A/C" : "M",
        "C/A" : "M",
        "C/G/T" : "B",
        "T/C/G" : "B",
        "C/T/G" : "B",
        "G/T/C" : "B",
        "T/G/C" : "B",
        "G/C/T" : "B",
        "A/G/T" : "D",
        "A/T/G" : "D",
        "T/A/G" : "D",
        "G/A/T" : "D",
        "G/T/A" : "D",
        "A/C/T" : "H",
        "A/T/C" : "H",
        "T/A/C" : "H",
        "T/C/A" : "H",
        "C/A/T" : "H",
        "C/T/A" : "H",
        "A/C/G" : "V",
        "A/G/C" : "V",
        "C/G/A" : "V",
        "C/A/G" : "V",
        "G/C/A" : "V",
        "G/A/C" : "V",
        "A/T/G/C" : "N",
        "A/T/C/G" : "N",
        "A/G/T/C" : "N",
        "A/G/C/T" : "N",
        "A/C/T/G" : "N",
        "A/C/G/T" : "N",
        "T/A/G/C" : "N",
        "T/A/C/G" : "N",
        "T/G/A/C" : "N",
        "T/G/C/A" : "N",
        "T/C/A/G" : "N",
        "T/C/G/A" : "N",
        "G/A/T/C" : "N",
        "G/A/C/T" : "N",
        "G/T/A/C" : "N",
        "G/T/C/A" : "N",
        "G/C/A/T" : "N",
        "G/C/T/A" : "N",
        "C/A/T/G" : "N",
        "C/A/G/T" : "N",
        "C/T/A/G" : "N",
        "C/T/G/A" : "N",
        "C/G/A/T" : "N",
        "C/G/T/A" : "N"
    }
    if '/' in poly:
        l_poly = poly.split('/')
    if poly in d_iupac_code.keys():
        poly_iupac = d_iupac_code[poly]
    elif poly in IUPACData.ambiguous_dna_letters:
        # already IUPAC
        poly_iupac = poly
    elif any(len(variant) > 1 for variant in l_poly):
        # indel poly, considering the first value to be the ref length
        poly_iupac = 'N' * len(l_poly[0])
    else:
        raise ValueError(f"Inapropriate polymorphism found {poly}")
    return poly_iupac

""" sequence_r = "TTGAAAGCTCA[A/G]TTCCATCATCCTCTTCTT"
sequence_y = "TTGAAAGCTCA[C/T]TTCCATCATCCTCTTCTT"
sequence_s = "TTGAAAGCTCA[G/C]TTCCATCATCCTCTTCTT"
sequence_w = "TTGAAAGCTCA[A/T]TTCCATCATCCTCTTCTT"
sequence_k = "TTGAAAGCTCA[G/T]TTCCATCATCCTCTTCTT"
sequence_m = "TTGAAAGCTCA[A/C]TTCCATCATCCTCTTCTT"
sequence_b = "TTGAAAGCTCA[C/G/T]TTCCATCATCCTCTTCTT"
sequence_d = "TTGAAAGCTCA[A/G/T]TTCCATCATCCTCTTCTT"
sequence_h = "TTGAAAGCTCA[A/C/T]TTCCATCATCCTCTTCTT"
sequence_v = "TTGAAAGCTCA[A/C/G]TTCCATCATCCTCTTCTT"
sequence_n = "TTGAAAGCTCA[A/G/C/T]TTCCATCATCCTCTTCTT"
result_r = replace_iupac(sequence_r)
result_y = replace_iupac(sequence_y)
result_s = replace_iupac(sequence_s)
result_w = replace_iupac(sequence_w)
result_k = replace_iupac(sequence_k)
result_m = replace_iupac(sequence_m)
result_b = replace_iupac(sequence_b)
result_d = replace_iupac(sequence_d)
result_h = replace_iupac(sequence_h)
result_v = replace_iupac(sequence_v)
result_n = replace_iupac(sequence_n)
print(result_r)
print(result_y)
print(result_s)
print(result_w)
print(result_k)
print(result_m)
print(result_b)
print(result_d)
print(result_h)
print(result_v)
print(result_n) """