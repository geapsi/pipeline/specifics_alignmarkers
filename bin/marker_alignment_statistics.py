#!/usr/bin/env python3
"""
Get statistics on the alignment
Usage: python3 marker_alignment_statistics.py -p alignment.paf -o stats
"""

import os
import argparse
import collections

import pandas as pd
import matplotlib.pyplot as plt

from paf_parsing import parse_paf_into_df
from paf_parsing import add_alignment_statistic_columns


parser = argparse.ArgumentParser(description = "Gets basic statistics from a paf \
    alignment file either before or after filtering")
parser.add_argument('-p', '--paf', help = 'paf alignement of markers (either the \
    original format or csv if --filtered_paf)', required=False)
parser.add_argument('-f', '--filtered_paf', help = 'whether the paf was filtered beforehand',
    required=False, default='False')
parser.add_argument('-o', '--outdir', help='name of the directory to output figures',
    required=False, default='stats')
args = parser.parse_args()

def histogram(value, title, outfile):
    """
    Histogram of number of appearances by markers
    """
    plt.figure()
    plt.hist(value, bins=100,color='orange',edgecolor='black',log=True)
    plt.xlabel('Number of appearances by markers')
    plt.ylabel('Log markers frequencies')
    plt.title(title)
    plt.rcParams['svg.fonttype'] = 'none'
    plt.savefig(outfile,format = 'svg')


def boxplot(df_stats, colum_variable, title, label_x, label_y, outfile):
    """
    Boxplot for a column given
    """
    plt.figure()
    df_stats.boxplot(column=colum_variable,sym='')
    plt.title(title)
    plt.xlabel(label_x)
    plt.ylabel(label_y)
    plt.rcParams['svg.fonttype'] = 'none'
    plt.savefig(outfile,format = 'svg')

if __name__ == '__main__':

    subdir = 'original_paf'
    if args.filtered_paf == 'True':
        subdir = 'filtered_paf'
        df_alignment = pd.read_csv(args.paf, sep=',', header=0)
    else:
        with open(args.paf, 'r', encoding='utf-8') as paf:
            df_alignment = parse_paf_into_df(paf)
    # add some basic stats to the df
    df_alignment = add_alignment_statistic_columns(df_alignment)

    selected_columns = ['qseqid','length','score','pident','plength']
    df_stats = df_alignment[selected_columns].copy()

    # OUTPUT DIRECTORY
    path = os.path.join(args.outdir, subdir)
    if not os.path.exists(path):
        os.makedirs(path, exist_ok=True)

    marker_count = df_stats.shape[0]
    # number of markers multialigned once, twice etc.
    series_marker_multialignment = df_stats['qseqid'].value_counts()
    series_count_marker_multialignment = series_marker_multialignment.value_counts()

    with open(path + '/marker_multialignment.txt', 'w', encoding='utf-8') as file_txt:
        file_txt.write(f'There is a total of {marker_count} alignments in the file\n')
        for number_of_multialignments, number_of_markers in \
            series_count_marker_multialignment.items():
            file_txt.write(f'There are {number_of_markers} markers that repeat\
                {number_of_multialignments} times in the file\n')

        # HISTOGRAMS
        histogram(series_marker_multialignment, 'Histogram of marker multialignment',
            os.path.join(path, 'histogram_multimapping_filter.svg'))
        # BOXPLOT
        boxplot(df_stats,['pident'], 'Percentage of identity of the query over the subject',
            '', '', os.path.join(path, 'boxplot_filtered_pident.svg'))
        boxplot(df_stats,['plength'], 'Percentage of alignment length of the query \
            over the subject', '', '', os.path.join(path, 'boxplot_filtered_plenght.svg'))