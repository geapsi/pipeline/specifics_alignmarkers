#!/usr/bin/env nextflow

process bedopsVcf2bed {

    container 'quay.io/biocontainers/bedops:2.4.41--h4ac6f70_1'

    input:
    tuple val(ID), path(vcf)
    output:
    tuple val(ID), path("${ID}.bed"), emit: bed

    script:
    """
    vcf2bed < $vcf > ${ID}.bed
    """
}