#!/usr/bin/env nextflow

process bedtoolsGetfasta {

    container 'quay.io/biocontainers/bedtools:2.31.0--h468198e_0'

    input:
    tuple val(ID), path(fasta), path(bed)

    output:
    tuple val(ID), path("${ID}.fa"), emit: fasta

    script:
    """
    bedtools \\
        getfasta \\
        -fi $fasta \\
        -bed $bed \\
        -nameOnly \\
        > ${ID}.fa
    """
}