#!/usr/bin/env nextflow

process samtoolsFaidx {

    container 'quay.io/biocontainers/samtools:1.17--hd87286a_1'

    input:
    tuple val(ID), path(fasta)

    output:
    tuple val(ID), path("*.fai"), emit: fai

    script:
    """
    samtools \\
        faidx \\
        $fasta
    """
}