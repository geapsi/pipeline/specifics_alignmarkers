#!/bin/usr/env nextflow


process fastaPolymorphismPosition {
	conda 'conda-forge::pandas=1.5.3 conda-forge::matplotlib=3.5.1 conda-forge::biopython=1.81'
	publishDir params.outdir + '/fastaPosPolymorphism', mode: params.publish_dir_mode
        input:
	path markers // fasta
        output:
        path 'markers_n.fa', emit: fasta_markers_N
	path 'marker_position_value.tsv', emit: tsv_markers
        script:
        """
	python3 ${params.project_dir}/bin/fasta_polymorphism_position.py \\
        -m $markers \\
        --output_fasta_n markers_n.fa \\
        --output_marker_position marker_position_value.tsv
        """
}

