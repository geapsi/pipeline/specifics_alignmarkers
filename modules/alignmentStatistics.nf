#!/usr/bin/env nextflow

process alignmentStatistics {
    conda 'pandas=1.3.5 matplotlib=3.5.1 conda-forge::biopython=1.81'
    publishDir params.outdir, mode: params.publish_dir_mode
    input:
    path paf
    val is_filtered

    output:
    path 'stats**'

    script:
    // def filtered = $is_filtered == 'true' ? 'True' : 'False'
    """
    python3 \\
        ${params.project_dir}/bin/marker_alignment_statistics.py \\
        --paf ${paf} \\
        --filtered_paf ${is_filtered} \\
        --outdir stats
    """
}