#!/usr/bin/env nextflow

process getCsvOfMarkers {

    container 'quay.io/biocontainers/biopython:1.75'

    input:
    tuple val(ID), path(fasta)

    output:
    tuple val(ID), path("${ID}.csv"), emit: csv

    script:
    """
    python3 ${params.project_dir}/bin/add_ref_alt_in_seq.py \\
        -fasta $fasta \\
        -slop_size ${params.slop_extend_size}\\
        -output ${ID}.csv
    """
}