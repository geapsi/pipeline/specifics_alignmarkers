#!/bin/usr/env nextflow


process csvToFasta {
	// conda 'conda-forge::pandas=1.3.5 conda-forge::matplotlib=3.5.1 conda-forge::biopython=1.81'
        container 'quay.io/biocontainers/python:3.10'
	publishDir params.outdir + '/fastaMarkers', mode: params.publish_dir_mode
        input:
	path csv
        output:
        path 'markers.fa', emit: fasta

        script:
        """
	python3 ${params.project_dir}/bin/csv_to_fasta.py \\
        -markers_csv $csv \\
        -sep ${params.csv_separator} \\
        -output_fasta markers.fa
        """
}

