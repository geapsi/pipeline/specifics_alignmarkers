#!/usr/bin/env nextflow

process bcftoolsAnnotate {

    container 'quay.io/biocontainers/bcftools:1.17--h3cc50cf_1'

    input:
    tuple val(ID), path(vcf)

    output:
    tuple val(ID), path("${ID}_annotated.vcf"), emit: vcf

    script:
    """
    id_pattern=\$(echo '%CHROM\\_%POS\\_%REF\\_%ALT')
    bcftools \\
        annotate \\
        --set-id +\$id_pattern \\
        $vcf \\
        -o ${ID}_annotated.vcf
    """
}
