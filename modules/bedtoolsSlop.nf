#!/usr/bin/env nextflow

process bedtoolsSlop {

    container 'quay.io/biocontainers/bedtools:2.31.0--h468198e_0'

    input:
    tuple val(ID), path(bed), path(sizes)

    output:
    tuple val(ID), path("${ID}_extended.bed"), emit: bed

    script:
    """
    bedtools \\
        slop \\
        -i $bed \\
        -g $sizes \\
        -b ${params.slop_extend_size} \\
        > ${ID}_extended.bed
    """
}