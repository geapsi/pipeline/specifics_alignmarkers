#!/usr/bin/env nextflow

process filterAlignment {
    conda 'conda-forge::pandas=1.3.5 conda-forge::matplotlib=3.5.1 conda-forge::biopython=1.81'
    publishDir params.outdir + '/filtered', mode: params.publish_dir_mode
    input:
    path fasta_markers
    path paf
    path polymorphism_markers
    val extra_opt

    output:
    path 'filtered_paf.csv', emit: filtered_paf
    path 'position_marker.tsv', optional: true
    path 'alignment_query_subject_sequences.txt', optional: true
    path 'unaligned_markers.log', optional: true

    script:
    """
    python3 \\
        ${params.project_dir}/bin/filter_alignment.py \\
        --markers ${fasta_markers} \\
        --alignment ${paf} \\
        --polymorphism_markers ${polymorphism_markers} \\
        --output_filtered_paf filtered_paf.csv \\
        --output_polymorphism_nature position_marker.tsv \\
        --output_alignment_sequences alignment_query_subject_sequences.txt \\
        --logs unaligned_markers.log \\
        ${extra_opt}
    """
}
