#!/usr/bin/env nextflow

process alignmentMinimap{
	// conda 'bioconda::minimap2=2.17'
	container 'quay.io/biocontainers/minimap2:2.26--he4a0461_1'
	publishDir  params.outdir + '/alignment', mode: params.publish_dir_mode
	input:
	path reference // fasta
	path query	// fasta

	output:
	path '*.paf', emit: paf

	script:
	def args = task.ext.args ?: ''
	"""
	minimap2 \\
		${args} \\
		--cs=long \\
		${reference} \\
		${query} \\
		> alignment.paf
	"""
}
